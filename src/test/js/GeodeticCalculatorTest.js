describe("GeodeticCalculator", function () {
	var assertEqual;
	
	beforeEach(function(){
		assertEquals = function(expected, actual, epsilon){
			var val = expected-actual;
			if(val<0){
				val*=-1;
			}
			jasmine.log(val);
			if(val>epsilon){
				return false;
			}
			return true;
		};			
	});
 
  it("testCalculateGeodeticCurve", function () {
		// instantiate the calculator
		var geoCalc = new GeodeticCalculator();

		// select a reference elllipsoid
		var reference = Ellipsoid.WGS84;

		// set Lincoln Memorial coordinates
		var lincolnMemorial = new GlobalCoordinates(38.88922, -77.04978);

		// set Eiffel Tower coordinates
		var eiffelTower = new GlobalCoordinates(48.85889, 2.29583);

		// calculate the geodetic curve
		var geoCurve = geoCalc.calculateGeodeticCurve(reference, lincolnMemorial, eiffelTower);

		expect(assertEquals(6179016.136, geoCurve.getEllipsoidalDistance(), 0.001)).toBe(true);
		expect(assertEquals(51.76792142, geoCurve.getAzimuth(), 0.0000001)).toBe(true);
		expect(assertEquals(291.75529334, geoCurve.getReverseAzimuth(), 0.0000001)).toBe(true);
	});
	
	it("testCalculateGeodeticMeasurement", function () {
		// instantiate the calculator
		var geoCalc = new GeodeticCalculator();

		// select a reference elllipsoid
		var reference = Ellipsoid.WGS84;

		// set Pike's Peak position
		var pikesPeak = new GlobalPosition(38.840511, -105.0445896, 4301.0);

		// set Alcatraz Island coordinates
		var alcatrazIsland = new GlobalPosition(37.826389, -122.4225, 0.0);

		// calculate the geodetic measurement
		var geoMeasurement = geoCalc.calculateGeodeticMeasurement(reference, pikesPeak, alcatrazIsland);

		expect(assertEquals(   -4301.0, geoMeasurement.getElevationChange(), 0.001)).toBe(true);
		expect(assertEquals( 1521788.826, geoMeasurement.getPointToPointDistance(), 0.001)).toBe(true);
		expect(assertEquals( 1521782.748, geoMeasurement.getEllipsoidalDistance(), 0.001)).toBe(true);
		expect(assertEquals(271.21039153, geoMeasurement.getAzimuth(), 0.0000001)).toBe(true);
		expect(assertEquals( 80.38029386, geoMeasurement.getReverseAzimuth(), 0.0000001)).toBe(true);
	});
	
	it("testAntiPodal1", function () {
		// instantiate the calculator
		var geoCalc = new GeodeticCalculator();

		// select a reference elllipsoid
		var reference = Ellipsoid.WGS84;

		// set position 1
		var p1 = new GlobalCoordinates(10, 80.6);

		// set position 2
		var p2 = new GlobalCoordinates(-10, -100);

		// calculate the geodetic measurement
		var geoCurve = geoCalc.calculateGeodeticCurve(reference, p1, p2);

		expect(assertEquals( 19970718.422432076, geoCurve.getEllipsoidalDistance(), 0.001)).toBe(true);
		expect(assertEquals(90.0004877491174, geoCurve.getAzimuth(), 0.0000001)).toBe(true);
		expect(assertEquals(270.0004877491174, geoCurve.getReverseAzimuth(), 0.0000001)).toBe(true);
	});
	
	it("testAntiPodal2", function () {
		// instantiate the calculator
		var geoCalc = new GeodeticCalculator();

		// select a reference elllipsoid
		var reference = Ellipsoid.WGS84;

		// set position 1
		var p1 = new GlobalCoordinates(11, 80);

		// set position 2
		var p2 = new GlobalCoordinates(-10, -100);

		// calculate the geodetic measurement
		var geoCurve = geoCalc.calculateGeodeticCurve(reference, p1, p2);

		expect(assertEquals( 19893320.272061437, geoCurve.getEllipsoidalDistance(), 0.001)).toBe(true);
		expect(assertEquals(360.0, geoCurve.getAzimuth(), 0.0000001)).toBe(true);
		expect(assertEquals(0.0, geoCurve.getReverseAzimuth(), 0.0000001)).toBe(true);
	});
	
	it("testInverseWithDirect", function () {
		// instantiate the calculator
		var geoCalc = new GeodeticCalculator();

		// select a reference elllipsoid
		var reference = Ellipsoid.WGS84;

		// set Lincoln Memorial coordinates
		var lincolnMemorial = new GlobalCoordinates(38.88922, -77.04978);

		// set Eiffel Tower coordinates
		var eiffelTower = new GlobalCoordinates(48.85889, 2.29583);

		// calculate the geodetic curve
		var geoCurve = geoCalc.calculateGeodeticCurve(reference, lincolnMemorial, eiffelTower);

		// now, plug the result into to direct solution
		var endBearing = [1];
		
		var dest = geoCalc.calculateEndingGlobalCoordinates(reference, lincolnMemorial, geoCurve.getAzimuth(), geoCurve.getEllipsoidalDistance(), endBearing);
		
		expect(assertEquals( eiffelTower.getLatitude(), dest.getLatitude(), 0.0000001 )).toBe(true);
		expect(assertEquals( eiffelTower.getLongitude(), dest.getLongitude(), 0.0000001 )).toBe(true);
	});
	
	it("testPoleCrossing", function () {
	 // instantiate the calculator
	 var geoCalc = new GeodeticCalculator();

	 // select a reference elllipsoid
	 var reference = Ellipsoid.WGS84;

	 // set Lincoln Memorial coordinates
	 var lincolnMemorial = new GlobalCoordinates(38.88922, -77.04978);

	 // set a bearing of 1.0deg (almost straight up) and a distance
	 var startBearing = 1.0;
	 var distance = 6179016.13586;

	 // set the expected destination
	 var expected = new GlobalCoordinates(85.60006433, 92.17243943);

	 // calculate the ending global coordinates
	 var dest = geoCalc.calculateEndingGlobalCoordinates(reference, lincolnMemorial, startBearing, distance );

	 expect(assertEquals(expected.getLatitude(), dest.getLatitude(), 0.0000001)).toBe(true);
	 expect(assertEquals(expected.getLongitude(), dest.getLongitude(), 0.0000001)).toBe(true);
	});
 
});
