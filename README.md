# javascriptgeodesylibrary
####  a geodesy library for JavaScript

**Examples** for this JavaScript library are listed at [projects home site](https://sites.google.com/site/javascriptgeodesylibrary).

The sources are hosted at [BitBucket](https://bitbucket.org/mjusix/javascriptgeodesylibrary)(GIT).

The library provides two important functions:
  * Calculate a destination given a starting point, direction, and distance traveled on Earth
  * Calculate the distance between two points on Earth

A very similar library exists for Java and C# at [Mike Gavaghans website](http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula-java/).
  
Feel free to use it for your project.
